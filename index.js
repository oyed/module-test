module.exports = class TwitchUser extends global.casthub.elements.user {

    /**
     * Initialize the new Module.
     */
    constructor() {
        super();

        /**
         * The Interval used to refresh the module data.
         *
         * @type {*}
         */
        this.interval = null;

        // Set the params for the User Module parent.
        this.color = '#FF0000';
        this.setCounts({
            followers: {
                name: 'Followers',
                count: -1,
                enabled: true,
            },
            subscribers: {
                name: 'Subscribers',
                count: -1,
                enabled: false,
            },
        });
    }

    /**
     * Run any asynchronous code when the Module is mounted to DOM.
     *
     * @return {Promise}
     */
    mounted() {
        return super.mounted().then(() => {
            return this.refresh();
        }).then(() => {
            this.interval = setInterval(() => this.refresh(), 10000);
        });
    }

    /**
     * Fetches fresh data from the Twitch API.
     *
     * @return {Promise}
     */
    refresh() {
        const data = this.interfaces.twitch.getData(this.identifier);

        if (data === null) {
            return Promise.reject('No Integration data present');
        }

        return this.interfaces.twitch.fetch({
            method: 'GET',
            url: 'kraken/channel',
            token: data.access_token,
        }).then(response => {
            this.avatar = response.logo;
            this.setCount('followers', response.followers);

            if (response.partner) {
                this.setCountEnabled('subscribers', true);

                return this.interfaces.twitch.fetch({
                    method: 'GET',
                    url: `kraken/channels/${this.identifier}/subscriptions`,
                    token: data.access_token,
                }).then(response => {
                    this.setCount('subscribers', response._total);
                });
            }
        });
    }

    /**
     * Perform any synchronous destruction, e.g. killing intervals.
     */
    destroy() {
        super.destroy();

        if (this.interval !== null) {
            clearInterval(this.interval);
        }
    }

};
